package simulation;

import java.awt.image.BufferedImage;
import java.util.HashMap;

public class Assets {
    public static HashMap<String, BufferedImage> wolves= new HashMap<>();
    public static HashMap<String, BufferedImage> sheeps= new HashMap<>();

    public static void init()
        {wolves.put("right",ImageLoader.loadImage("../textures/WOLFRIGHT.png"));
            wolves.put("left",ImageLoader.loadImage("../textures/WOLF-LEFT.png"));
            wolves.put("hungry-right",ImageLoader.loadImage("../textures/HU-WOLF-RIGHT.png"));
            wolves.put("hungry-left",ImageLoader.loadImage("../textures/HU-WOLF-LEFT.png"));
        sheeps.put("right",ImageLoader.loadImage("../textures/SHEEP-RIGHT.png"));
        sheeps.put("left",ImageLoader.loadImage("../textures/SHEEP-LEFT.png"));

        }

}
