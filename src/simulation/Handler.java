package simulation;

public class Handler {
    public static Game game;

    public Handler(Game game) {this.game=game;}

    public static Display getDisplay() {return Game.getDisplay();}

    public static Game getGame() {
        return game;
    }
}
