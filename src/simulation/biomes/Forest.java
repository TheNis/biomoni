package simulation.biomes;

import simulation.livingbeings.animals.Sheep;
import simulation.livingbeings.animals.Wolf;
import simulation.livingbeings.plants.AppleTree;

public class Forest extends Biome{
    public Forest(int x, int y)
        {super(x,y);
            int randomFactor=(int)Math.floor(Math.random()*75)+75;
            for(int i=0; i<randomFactor; i++)
                livingBeings.add(new Sheep((int)Math.floor(Math.random()*(width))+x,(int)Math.floor(Math.random()*(height))+y,this));
            randomFactor=(int)Math.floor(Math.random()*7)+8;
            for(int i=0; i<randomFactor; i++)
                livingBeings.add(new Wolf((int)Math.floor(Math.random()*(width))+x,(int)Math.floor(Math.random()*(height))+y,this));
            randomFactor=(int)Math.floor(Math.random()*20)+20;
            for(int i=0; i<randomFactor; i++)
                livingBeings.add(new AppleTree((int)Math.floor(Math.random()*(width))+x,(int)Math.floor(Math.random()*(height))+y,this));

        }
}
