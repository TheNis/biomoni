package simulation.biomes;

import simulation.Game;
import simulation.livingbeings.animals.Sheep;
import simulation.livingbeings.animals.Wolf;
import simulation.livingbeings.fathers.LivingBeing;
import simulation.livingbeings.plants.AppleTree;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class Biome {
    protected int absX, absY, x, y, width, height;
    protected ArrayList<LivingBeing> livingBeings;
    private ArrayList<LivingBeing> fakeLivingBeings;
    private boolean onOff=false;

    public void setOn() {
        onOff=true;
    }

    public void setOff()
        {onOff=false;}
    public void clearLivingBeing()
        {livingBeings.clear(); fakeLivingBeings.clear();}

    public ArrayList<LivingBeing> getLivingBeings() {
        return livingBeings;
    }

    public Biome(int x, int y)
        {absX=x; absY=y;
            fakeLivingBeings = new ArrayList<>();
        livingBeings = new ArrayList<>();
        width=1280; height=720;}

    public int getAbsX() {
        return absX;
    }

    public int getAbsY() {
        return absY;
    }

    public void tick()
        {
            x=absX+Game.offsetX;
            y=absY+Game.offsetY;
            Iterator<LivingBeing> I= fakeLivingBeings.iterator();

            while (I.hasNext())
                try {
                    LivingBeing a=I.next();
                    a.tick();
                    if(a.isDead())
                        livingBeings.remove(a);


                }
                catch (Exception e)
                {
                    System.out.println(livingBeings.size());
                    e.printStackTrace();
                    System.exit(0);}

        }

    public void render(Graphics g)

        {   //g.setColor(Color.black);
            //g.drawRect(x,y,width,height);
            fakeLivingBeings = new ArrayList<>(livingBeings);
            Iterator<LivingBeing> I= fakeLivingBeings.iterator();
            while (I.hasNext()) {
                LivingBeing a=I.next();
                try {
                    a.render(g);
                } catch (Exception e) {
                    System.exit(0);
                }
            }
        }

    public int getWidth() {
        return width;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isOn() {
        return onOff;
    }

    public int getHeight() {
        return height;
    }
}
