package simulation;

import simulation.biomes.Biome;
import simulation.biomes.Forest;
import simulation.livingbeings.animals.Sheep;
import simulation.livingbeings.animals.Wolf;
import simulation.livingbeings.fathers.LivingBeing;
import simulation.livingbeings.fathers.animals.Animal;
import simulation.livingbeings.plants.AppleTree;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Game implements Runnable, MouseMotionListener, MouseListener, KeyListener {
    public static int mouseX, mouseY, beforeX, beforeY, keyTyped =KeyEvent.VK_UNDEFINED, foffsetX=0, foffsetY=0, offsetX=0, offsetY=0;
    public static boolean graphicUtilities =false;
    Scanner in=new Scanner(System.in);
    private Handler handler=new Handler(this);
    private static Display display;
    private int frames=0;
    private boolean running=false;
    private final int TPS=20;
    private Thread thread;
    private BufferStrategy bs;
    private Graphics g;
    private World world;


    public static void main(String[] args) {
        new Game().start();
    }

    public void tick() {
        /*switch (keyTyped)
        {//case KeyEvent.VK_E: biome.clearLivingBeing(); break;
            case KeyEvent.VK_S: world.addLivingBeing(new Sheep(mouseX -16, mouseY -16, biome)); break;
            case KeyEvent.VK_W: world.addLivingBeing(new Wolf(mouseX -16, mouseY -16, biome)); break;
            case KeyEvent.VK_ALT:graphicUtilities =!graphicUtilities; break;
            case KeyEvent.VK_T: world.addLivingBeing(new AppleTree(mouseX-32,mouseY-32, biome)); break;
            case KeyEvent.VK_R: world=new World(); offsetY=0; offsetX=0; break;}
        keyTyped =KeyEvent.VK_UNDEFINED;
        biome.tick();
*/
        offsetX=foffsetX;
        offsetY=foffsetY;


world.tick();
    }

    public void render()
    {
        bs=display.getCanvas().getBufferStrategy();
        if(bs==null) {
            display.getCanvas().createBufferStrategy(5);
            return;
        };
        g =bs.getDrawGraphics();
        g.clearRect(0,0,display.getCanvas().getWidth(),display.getCanvas().getHeight());
        g.setColor(new Color(41,163,41));
        g.fillRect(0,0,display.getCanvas().getWidth(),display.getCanvas().getHeight());
        //inizio disegno

world.render(g);
      //  sButton.render(g);
        //wButton.render(g);
        //fine disegno
        bs.show();
        g.dispose();
    }

    public void init() {
        Assets.init();
        display = new Display("Biomoni");
        handler = new Handler(this);
        //display.getCanvas().addMouseListener(sButton);
        //display.getCanvas().addMouseListener(wButton);
        display.getCanvas().addMouseMotionListener(this);
        display.getFrame().addKeyListener(this);
        world=new World();
    }


    @Override
    public void run() {
        init();
        double timePerTick = 1000000000 / TPS;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        while(running) {
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;
            if (delta >= 1) {
                tick();
                delta--;
            }
            render();
            frames++;

            if (timer >= 1000000000) {
                System.out.println(frames);

                frames = 0;
                timer = 0;
            }
        }
    }

    public synchronized void start()
    {   if(running)
        return;
        running=true;
        thread=new Thread(this);
        thread.start();

    }

    public synchronized void stop()
    { if(!running)
        return;
        running=false;
        try
        {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Display getDisplay() {
        return display;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
    keyTyped =e.getKeyCode();}

    @Override
    public void keyReleased(KeyEvent e) {
    if(e.getKeyCode()==KeyEvent.VK_ESCAPE)
        System.exit(0);}

    @Override
    public void mouseDragged(MouseEvent e) {

        foffsetX=e.getX()-mouseX;
        foffsetY=e.getY()-mouseY;

    }

    @Override
    public void mouseMoved(MouseEvent e) {
    mouseX =e.getX()-foffsetX;
    mouseY =e.getY()-foffsetY;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
    beforeX=e.getX();
    beforeY=e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}