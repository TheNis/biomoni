package simulation;

import javax.swing.*;
import java.awt.*;

public class Display {
    private JFrame frame;

    private Canvas canvas;
    private String title;
    public static final int WIDTH=1280, HEIGHT=720;
    public Display(String title)
    {
        this.title=title;
        createWindow();
    }

    private void createWindow(){
        frame=new JFrame(title);
        frame.setSize(WIDTH,HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(WIDTH,HEIGHT));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setIconImage(Assets.sheeps.get("right"));

        canvas=new Canvas();
        canvas.setPreferredSize(new Dimension(WIDTH,HEIGHT));
        canvas.setMinimumSize(new Dimension(WIDTH,HEIGHT));
        canvas.setFocusable(false);

        frame.add(canvas);
        frame.pack();

    }

    public Canvas getCanvas() {
        return canvas;
    }

    public JFrame getFrame() {
        return frame;
    }

}