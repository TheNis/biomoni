package simulation.livingbeings.plants;

import simulation.Game;
import simulation.biomes.Biome;
import simulation.livingbeings.fathers.plants.Fruit;

import java.awt.*;


public class Apple extends Fruit {


    public Apple(int x, int y, Biome biome)
        {super(x,y, biome); poisonous=false; nutritionValue=(int)Math.floor(Math.random()*2)+1;}

    @Override
    public void render(Graphics g) {
        g.setColor(Color.YELLOW);
        if(mature)
            g.setColor(Color.RED);
        if(poisonous)
            g.setColor(Color.GREEN);
        g.fillRect(x,y, width,height);

    }

    @Override
    public void logic() {
        if(!mature)
            growingFactor+= Math.floor(Math.random()*3);
        else
            consumable=true;
        if(growingFactor>=150)
            mature=true;
        if (mature && absY-initialY<=40)
            absY++;
        if(y!=initialY && poisonous==false)
            if(Math.floor(Math.random()*1000)==42)
                poisonous=true;
        if(poisonous && Math.floor(Math.random()*500)==42)
            die();
    }
}
