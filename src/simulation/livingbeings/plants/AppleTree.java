package simulation.livingbeings.plants;

import simulation.Handler;
import simulation.biomes.Biome;
import simulation.livingbeings.fathers.plants.Fruit;
import simulation.livingbeings.fathers.plants.Tree;

import java.awt.*;
import java.util.Iterator;

public class AppleTree extends Tree {

    public AppleTree(int x, int y, Biome biome)
        {super(x, y, biome);
        fruitType= Fruit.Fruits.APPLE;
        yieldingFactor=200;}

    @Override
    public void render(Graphics g)
        {g.setColor(new Color(96, 65, 18));
        g.fillRect(x+width/4,y+height/2,width/2,height/2);
        g.setColor(new Color(26, 96, 11));
        g.fillRect(x,y,width,height/2);}

}
