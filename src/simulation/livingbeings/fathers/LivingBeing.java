package simulation.livingbeings.fathers;

import simulation.Game;
import simulation.biomes.Biome;

import java.awt.*;

public abstract class LivingBeing {
    protected int x, y, absX, absY, width, height, nutritionValue;
    protected boolean death=false;
    protected Biome biome;
    public int getNutritionValue() {
        return nutritionValue;
    }
    public abstract void render(Graphics g);
    public void tick()
        { x= absX+ Game.offsetX;
         y=absY+Game.offsetY;
         if(biome.isOn())
             logic();
        }

    public abstract void logic();

    public LivingBeing(int x, int y, Biome biome)
        {this.biome=biome; absX=x; absY=y;}

    public boolean isDead() {
        return death;
    }

    public void die()
        {death=true;}

    public int getX() {return x;}

    public int getY() {return y;}

    public int getWidth() {return width;}

    public int getHeight() {return height;}

    public static Point livingBeingCenter(LivingBeing livingBeing)
    {return new Point(livingBeing.getX()+livingBeing.getWidth()/2, livingBeing.getY()+livingBeing.getHeight()/2);}

    public static int distanceBetween2LivingBeing(LivingBeing lv1, LivingBeing lv2)
    {return (int)Math.hypot(livingBeingCenter(lv1).getX()-livingBeingCenter(lv2).getX(),livingBeingCenter(lv1).getY()-livingBeingCenter(lv2).getY());}


}
