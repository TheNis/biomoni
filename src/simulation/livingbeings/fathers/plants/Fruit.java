package simulation.livingbeings.fathers.plants;

import simulation.biomes.Biome;
import simulation.livingbeings.plants.Apple;

public abstract class Fruit extends Plant{
    protected int initialY, growingFactor;
    protected boolean mature;

    public Fruit(int x, int y, Biome biome)
        {super(x,y,biome); initialY=y;
        consumable=false;
        mature=false;
        width=8; height=8;}


    public enum Fruits
        {APPLE;

        public Fruit getFruitByType(int x,int y, Biome biome)
        {switch (this)
            {case APPLE: return new Apple(x, y, biome);
            default: return new Apple(x,y, biome);}}}
}
