package simulation.livingbeings.fathers.plants;

import simulation.Handler;
import simulation.biomes.Biome;

import java.util.ArrayList;

public abstract class Tree extends Plant{
    protected Fruit.Fruits fruitType;
    protected int yieldingFactor;

    public Tree(int x, int y, Biome biome)
        {super(x,y,biome);
            consumable=false;
        width=64; height=64;}

    protected void yield()
        {if(Math.floor(Math.random()*yieldingFactor)==1)
            biome.getLivingBeings().add(fruitType.getFruitByType(absX+(int)Math.floor(Math.random()*(width-8)), absY+(int)Math.floor(Math.random()*(height-32)),biome)); }

    @Override
    public void logic()
        {
            yield();}
}
