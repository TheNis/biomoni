package simulation.livingbeings.fathers.plants;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.LivingBeing;

public abstract class Plant extends LivingBeing {
protected boolean poisonous, consumable;

    public Plant(int x, int y, Biome biome) {
        super(x, y, biome);
    }

    public boolean isConsumable() {
        return consumable;
    }

    public boolean isPoisonous() {
        return poisonous;
    }


}