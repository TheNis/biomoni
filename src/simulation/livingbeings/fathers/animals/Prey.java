package simulation.livingbeings.fathers.animals;

import simulation.Handler;
import simulation.biomes.Biome;
import simulation.livingbeings.fathers.LivingBeing;
import simulation.livingbeings.fathers.plants.Plant;

import java.awt.*;
import java.util.Iterator;

public abstract class Prey extends Animal {
    protected Animal predator;
    protected Plant food;

    public Prey(int x, int y, Biome biome) {
        super(x, y, biome);
    }

    @Override
    public void logic()
        {super.logic();
            Iterator<LivingBeing> I= biome.getLivingBeings().iterator();

            //controlla tutti gli esseri viventi all'interno del gioco finchè non trova un predatore
        while(I.hasNext() && predator==null)
            {LivingBeing lv = I.next();

                //se sono piante, se si possono mangiare e sono all'interno del suo campo visivo le assegna come cibo
            if(lv instanceof Plant)
                 {Plant p=(Plant)lv;
                 if(distanceBetween2LivingBeing(this,p)<=visualField && p.isConsumable())
                    food=p;}

            if(lv instanceof Animal)
                {Animal a=(Animal) lv;
                if(a!=this && distanceBetween2LivingBeing(this,a)<=visualField )

                //se sono della stessa classe(es. pecora e pecora) e non hanno un partner, si assegnano come partner
                {if(a.getClass()==getClass() && partner==null && (a.getPartner()==null || a.getPartner()==this))
                        {partner=a; a.partner=this;}

                    //se sono dei predatori se li assegnano come predatore
                    if(a instanceof Predator)
                        predator=a;}}}



        /*controlla se ha un predatore e se è nel suo campo visivo scappa e continua a scappare finché non è alla distanza doppio
        del suo campo visivo, a questo punto non ha più un predatore*/
        if(predator!=null)
            {if(distanceBetween2LivingBeing(this, predator)<=visualField*2)
                {getAwayFrom(predator);}
            else predator=null;}

        /*se ha fame controlla la distanza tra lui e il cibo e se ci è abbastanza vicino lo mangia,
        se è velenoso(o marcio) il cibo invece che dargli fame, gliela toglie.
        se invece il è ancora lontano dal cibo lo segue.*/
        else if(food!=null && hunger<25)
            {int distance=distanceBetween2LivingBeing(food, this);
            if(distance<=32)
                {food.die();
                if(food.isPoisonous())
                    hunger=Math.max(0, hunger-food.getNutritionValue());
                else hunger=Math.min(hunger+food.getNutritionValue(),maxHunger);
                    food=null;}
                else if(distance<=visualField)
                    follow(food);
                else food=null;}

        //si spiega da sola, andare a vedere le funzioni
        else if(canMakeChild())
            makeChild();

        //andare a vedere le funzioni x2
        else move();}

    @Override
    public void showGraphicUtilities(Graphics g)
        {Point center=livingBeingCenter(this);
        g.setColor(Color.YELLOW);
        g.drawOval((int)center.getX() - visualField,(int)center.getY()  - visualField, visualField * 2, visualField * 2);
        g.drawOval((int)center.getX() - 5 / 2, (int)center.getY() - 5 / 2, 5, 5);
        if(predator!=null)
            g.drawLine((int)center.getX(), (int)center.getY(), (int)livingBeingCenter(predator).getX(), (int)livingBeingCenter(predator).getY());
        g.setColor(Color.PINK);
        if(partner!=null)
            {if (parent)
                g.setColor(Color.CYAN);
            g.drawLine((int)center.getX(), (int)center.getY(), (int)livingBeingCenter(partner).getX(), (int)livingBeingCenter(partner).getY());
              }
            g.setColor(Color.BLACK);
            g.drawString(hunger+"",x+width/2-g.getFontMetrics().stringWidth(hunger+"")/2,y+height/2);}

}
