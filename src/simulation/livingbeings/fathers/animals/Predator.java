package simulation.livingbeings.fathers.animals;

import simulation.Handler;
import simulation.biomes.Biome;
import simulation.livingbeings.fathers.LivingBeing;

import java.awt.*;
import java.util.Iterator;

public abstract class Predator extends Animal {
    protected Animal prey;

    public Predator(int x, int y, Biome biome) {
        super(x, y, biome);
    }

    @Override
    public void logic()
        {       super.logic();
            Iterator<LivingBeing> I= biome.getLivingBeings().iterator();

            //controlla tutti gli esseri viventi che ci sono finchè non trova una preda
        while(I.hasNext() && prey==null)
            {LivingBeing lv = I.next();
                if(lv instanceof Animal)
                    {Animal a=(Animal) lv;
                    if(a!=this && distanceBetween2LivingBeing(this,a)<=visualField )

                    //se sono della stessa classe(es. pecora e pecora) e non hanno un partner, si assegnano come partner
                        {if(a.getClass()==getClass() && partner==null && (a.getPartner()==null || a.getPartner()==this))
                            {partner=a; a.partner=this;}

                            //altrimenti se è una preda diventa la sua.
                        if(a instanceof Prey)
                            prey=a;}}}



        //se ha fame ed ha una preda controlla la distanza tra di loro e se è abbastanza vicino la mangia altrimente la insegue
        if(prey!=null && hunger<16)
            {int distance=distanceBetween2LivingBeing(this,prey);
                if (distance<=30)
                    {prey.die();
                    hunger=Math.min(prey.getNutritionValue()+hunger,20);
                    prey=null;}
                else if (distance<= visualField)
                    {follow(prey);}
                else
                    prey=null;}
        else
            {if(hunger>=16)
                prey=null;
            if(canMakeChild())
                makeChild();
            else
                move();}}

    @Override
    public void showGraphicUtilities(Graphics g)
        {Point center=livingBeingCenter(this);
        g.setColor(Color.RED);
        g.drawOval((int)center.getX() - visualField,(int)center.getY()  - visualField, visualField * 2, visualField * 2);
        g.drawOval((int)center.getX() - 5 / 2, (int)center.getY() - 5 / 2, 5, 5);
        if (prey != null)
            g.drawLine((int)center.getX(), (int)center.getY(), (int)livingBeingCenter(prey).getX(), (int)livingBeingCenter(prey).getY());
        g.setColor(Color.PINK);
        if(partner!=null)
            {if (parent)
                g.setColor(Color.CYAN);
                g.drawLine((int)center.getX(), (int)center.getY(), (int)livingBeingCenter(partner).getX(), (int)livingBeingCenter(partner).getY()); }
            g.setColor(Color.WHITE);
            g.drawString(hunger+"",x+width/2-g.getFontMetrics().stringWidth(hunger+"")/2,y+height/2);
        }

}

