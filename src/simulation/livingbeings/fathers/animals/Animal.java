package simulation.livingbeings.fathers.animals;

import simulation.Game;
import simulation.Handler;
import simulation.biomes.Biome;
import simulation.livingbeings.animals.Sheep;
import simulation.livingbeings.animals.Wolf;
import simulation.livingbeings.fathers.LivingBeing;

import java.awt.*;

public abstract class Animal extends LivingBeing {
    protected int directionX=(int)Math.floor(Math.random()*3)-1, directionY=(int)Math.floor(Math.random()*3)-1,
            maxHunger, hunger, visualField, speed,
            reproductionRate;
    protected Animals animalType;
    protected Animal partner;
    protected boolean parent=false, poisoned;

    public Animal(int x, int y, Biome biome) {
        super(x, y, biome);
    }


    public abstract void showGraphicUtilities(Graphics g);

    @Override
    public void logic()
        {
            if(partner!=null && partner.isDead())
            partner=null;
        if(partner!=null && Math.floor(Math.random()*reproductionRate)==4)
            {partner.partner=null;partner.parent=false;
            partner=null; parent=false;}
        if(Math.floor(Math.random()*100)==42)
            hunger=Math.max(hunger-(int)Math.floor(Math.random()*5),0);
        if(hunger<=0)
            die();}

    @Override
    public void render(Graphics g)
        {if(Game.graphicUtilities)
            showGraphicUtilities(g);}

    public Animal getPartner() {
        return partner;
    }

    public boolean isParent() {
        return parent;
    }

    public void becomeParent() {parent=true;}


    protected void follow(LivingBeing lv) {followOrGetAway(lv, 1);}

    protected void getAwayFrom(LivingBeing lv) {followOrGetAway(lv,-1);}

    private void followOrGetAway(LivingBeing lv, int direction)
        {if (livingBeingCenter(lv).getX()> livingBeingCenter(this).getX())
            {absX += speed*direction; directionX=1*direction;}
        else if (livingBeingCenter(lv).getX()< livingBeingCenter(this).getX())
            {absX -= speed*direction; directionX=-1*direction;}
        if (livingBeingCenter(lv).getY() > livingBeingCenter(this).getY())
            {absY += speed*direction; directionY=1*direction;}
        else if (livingBeingCenter(lv).getY() < livingBeingCenter(this).getY())
            {absY -= speed*direction; directionY=-1*direction;}}

    protected boolean canMakeChild()
        {return partner!=null && !parent && !partner.isParent() && hunger>(double)7/10*maxHunger && partner.hunger>(double)7/10*partner.maxHunger;}

    protected void makeChild()
        {int distance=distanceBetween2LivingBeing(this, partner);
        if(distance<=width)
            {becomeParent(); partner.becomeParent(); biome.getLivingBeings().add(animalType.getAnimalByType(x, y, biome));}
        else if (distance <= visualField)
            follow(partner);
        else
            {partner.partner=null; partner=null;}}

    protected void move()
        {if(Math.floor(Math.random()*100)==42)
            {directionX=(int)Math.floor(Math.random()*3)-1;
            directionY=(int)Math.floor(Math.random()*3)-1;}
        if(Math.floor(Math.random()*8)==4)
            {absX+=speed*directionX; absY+=speed*directionY; }
}


    public enum Animals
        {SHEEP, WOLF;

        public Animal getAnimalByType(int x, int y, Biome biome)
            {switch (this)
                {case WOLF: return new Wolf(x, y, biome);
                case SHEEP: return  new Sheep(x, y, biome);
                default: return new Sheep(x, y, biome);}}}

}
