package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Predator;

import java.awt.*;

public class Lion extends Predator {
    public Lion(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=130;
        animalType=Animals.LION;
        speed=15;
        width=32;
        height=32;
        maxHunger=25;
        reproductionRate=1400;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        lostHungerRate=5;
    }
    public void tick() {
        super.tick();

    }
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(166, 83, 26));
        g.fillRect(x,y,width,height);
    }
}
