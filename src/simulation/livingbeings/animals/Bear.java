package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Predator;

import java.awt.*;

public class Bear extends Predator {
    public Bear(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=120;
        animalType=Animals.BEAR;
        speed=14;
        width=32;
        height=32;
        maxHunger=30;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        reproductionRate=1600;
        lostHungerRate=4;
    }
    public void tick() {
        super.tick();

    }
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(72, 48, 24));
        g.fillRect(x,y,width,height);
    }
}
