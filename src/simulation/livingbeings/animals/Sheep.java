package simulation.livingbeings.animals;

import simulation.Assets;
import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Prey;

import java.awt.*;

public class Sheep extends Prey {

    public Sheep(int x, int y, Biome biome)
        {super(x,y,biome);
        visualField=200;
        animalType= Animals.SHEEP;
        speed=10;
        width=32;
        height=32;
        reproductionRate=500;
        maxHunger=30;
        hunger=(int)Math.floor(Math.random()*(maxHunger-15))+15;
        nutritionValue=(int)Math.floor(Math.random()*5)+1;}

    @Override
    public void render(Graphics g)
        {g.setColor(Color.WHITE);
        if(directionX>0)
            g.drawImage(Assets.sheeps.get("right"), x, y,width, height, null);
        else
            g.drawImage(Assets.sheeps.get("left"), x, y,width, height, null);

            super.render(g);}

}
