package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Prey;

import java.awt.*;

public class Boar extends Prey {
    public Boar(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=150;
        animalType=Animals.BOAR;
        speed=9;
        width=32;
        height=32;
        reproductionRate=700;
        maxHunger=30;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        nutritionValue=(int)Math.floor(Math.random()*5)+2;
        lostHungerRate=5;
    }
    public void tick() {
        super.tick();

    }
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(27, 30, 33));
        g.fillRect(x,y,width,height);
    }
}
