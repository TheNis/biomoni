package simulation.livingbeings.animals;

import simulation.Assets;
import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Animal;
import simulation.livingbeings.fathers.animals.Predator;

import java.awt.*;

public class Wolf extends Predator {
    private Animal prey;

    public Wolf(int x, int y, Biome biome)
        {super(x,y,biome);
        visualField=120;
        animalType= Animals.WOLF;
        speed=15;
        width=32;
        height=32;
        maxHunger=20;
        hunger=(int)Math.floor(Math.random()*(maxHunger-10))+10;
        reproductionRate=1500;}

    @Override
    public void logic()
        {super.logic();
        if(hunger<=5) {speed=22; visualField=350;}
        else {speed=15; visualField=120;}}

    @Override
    public void render(Graphics g)
        {if(directionX>0)
            {if (hunger <= 5) g.drawImage(Assets.wolves.get("hungry-right"), x, y, width, height, null);
            else g.drawImage(Assets.wolves.get("right"), x, y, width, height, null);}
        else
            {if (hunger <= 5) g.drawImage(Assets.wolves.get("hungry-left"), x, y, width, height, null);
            else g.drawImage(Assets.wolves.get("left"), x, y, width, height, null);}
           super.render(g);}


}
