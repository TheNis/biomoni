package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Prey;

import java.awt.*;

public class Bunny extends Prey {

    public Bunny(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=190;
        animalType=Animals.BUNNY;
        speed=15;
        width=32;
        height=32;
        reproductionRate=500;
        maxHunger=15;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        nutritionValue=(int)Math.floor(Math.random()*3)+1;
        lostHungerRate=5;
    }
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(44, 47, 36));
        g.fillRect(x,y,width,height);
    }
}
