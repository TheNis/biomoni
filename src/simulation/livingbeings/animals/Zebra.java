package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Prey;

import java.awt.*;

public class Zebra extends Prey{
    public Zebra(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=160;
        animalType=Animals.ZEBRA;
        speed=10;
        width=32;
        height=32;
        maxHunger=30;
        reproductionRate=600;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        nutritionValue=(int)Math.floor(Math.random()*5)+2;
        lostHungerRate=5;
    }
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(, , ));
        g.fillRect(x,y,width,height);
    }

}
