package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Prey;

import java.awt.*;

public class Deer extends Prey {
    public Deer(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=170;
        animalType=Animals.DEER;
        speed=11;
        width=32;
        height=32;
        maxHunger=30;
        reproductionRate=800;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        nutritionValue=(int)Math.floor(Math.random()*7)+4;
        lostHungerRate=4;
    }
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(75, 54, 28));
        g.fillRect(x,y,width,height);
    }
}
