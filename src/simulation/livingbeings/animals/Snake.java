package simulation.livingbeings.animals;

import simulation.biomes.Biome;
import simulation.livingbeings.fathers.animals.Predator;

import java.awt.*;

public class Snake extends Predator {
    public Snake(int x, int y, Biome biome) {
        super(x, y, biome);
        visualField=150;
        animalType=Animals.SNAKE;
        speed=13;
        width=32;
        height=32;
        maxHunger=40;
        hunger=(int)Math.floor(Math.random()*(maxHunger-maxHunger/2))+maxHunger/2;
        reproductionRate=2000;
        lostHungerRate=2;
    }

    @Override
    public void tick() {
        super.tick();
        if(hunger>=30) {
            speed=8;
        } else if (hunger<=8){
            speed=16;
        }
        else
            speed=13;
    }

    @Override
    public void render(Graphics g) {
        super.render(g);
        g.setColor(new Color(9, 121, 9));
        g.fillRect(x,y,width,height);
    }
}
