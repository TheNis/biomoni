package simulation;

import simulation.biomes.Biome;
import simulation.biomes.Forest;
import simulation.livingbeings.fathers.LivingBeing;
import simulation.livingbeings.fathers.animals.Animal;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class World {
    private HashMap<Point,Biome> biomes = new HashMap<>();
    private HashMap<Point,Biome> fakeBiomes = new HashMap<>();

    /*public void addLivingBeing()
        {
            Iterator<Biome> I=biomes.values().iterator();
            while (I.hasNext()) {
                Biome b=I.next();
                if(b.isOn())
                {b.getLivingBeings().add(lv);
                return;
                }}}
    */
    public World() {
        biomes.put(new Point(0,0),new Forest(0,0));
        biomes.get(new Point(0,0)).setOff();
    }

    public void tick() {
        loadChunk();
        Iterator<Biome> I=biomes.values().iterator();
        while (I.hasNext()) {
            Biome b=I.next();
                b.tick();
        }
    }

    public void render(Graphics g)
    { Iterator<Biome> I=biomes.values().iterator();
        while (I.hasNext())
        {Biome b=I.next();
            if(b.isOn())
                b.render(g);}
            }

    private void loadChunk()
        {for(Map.Entry<Point, Biome> entry : biomes.entrySet()){
            if (new Rectangle(0-Display.WIDTH/2,0-Display.HEIGHT/2,Display.WIDTH*2,Display.HEIGHT*2).intersects(entry.getValue().getX(), entry.getValue().getY(), entry.getValue().getWidth(), entry.getValue().getHeight()))
                entry.getValue().setOn();
            else
                entry.getValue().setOff();
            if(entry.getValue().isOn())
            {createChunk(entry.getValue(), entry.getValue().getAbsX()+entry.getValue().getWidth(),
                  entry.getValue().getAbsY());
            createChunk(entry.getValue(),entry.getValue().getAbsX()-entry.getValue().getWidth(),
                    entry.getValue().getAbsY());
            createChunk(entry.getValue(), entry.getValue().getAbsX(),
                    entry.getValue().getAbsY()+entry.getValue().getHeight());
            createChunk(entry.getValue(), entry.getValue().getAbsX(),
                    entry.getValue().getAbsY()-entry.getValue().getHeight());
            }}
        biomes.putAll(fakeBiomes);
        }



        private void createChunk(Biome b, int newX, int newY)
        {
            if (!biomes.containsKey(new Point(newX, newY))) {
            fakeBiomes.put(new Point(newX,newY), new Forest(newX,newY));}}

}
