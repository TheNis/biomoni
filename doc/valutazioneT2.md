## GRUPPO 2 
Cremonese - Dondi - Facchini - pietrafitta - Pecchi

"INFINITE BIOME"


# Valutazione Presentazione e Prodotto (4,5 su 5, Peer 3,7)

Il prodotto si distingue per alcune peculiarità, come ad esempio 
* L'aspetto ludico anni 80 per la parte di avvio
* Numero di ambienti "infiniti" che vengono creati all'istante e se non visualizzati si "addormentano". 
* Uso del suono
* Complessa gestione degli animali

La demo è risultata interessante, anche se non tutti sono intervenuti con la stessa tempistica e/o efficacia - evidenziando un po' l'aspetto del "dittatore illuminato" che caratterizza il gruppo. Alcuni "glitch" hanno po' compromesso l'effetto generale del programma. 


La presentazione è stata complessivamente efficace e a tratti coinvolgente. 


# Valutazione ultima consegna (4 su 5)

L'UML è stato buono sin da subito, cosa che ha probabilmente aiutato la realizzazione del progetto
Manca qualche documento di pianificazione.
Il codice è ben organizzato e le responsabilità sono ripartite correttamente
Nessun Javadoc

L'uso di git evidenzia un uso un po' sbilanciato di git, con Cremonese e Pecchi che hanno assieme il 66% dei commit. Pur col fatto che git non gestisce il lavoro in coppia, si tratta di un aspetto un po' strano.

    10  The Nis
     9  Denis Cremonese
     9  pecchi lorenzo
     6  FaccoCris
     4  pietrafitta alessandro
     2  dondi marco
     1  Piffy@home

Si deduce che pecchi ha lavorato di più sui singoli animali, per il resto il lavoro di upload è stato fatto da Cremonese. 

#Valutazone complessiva

Il gruppo pare abbanstanza affiatato, con una buona divisione dei lavori, pur evidenziando un contributo più elevato da parte di Cremonese. Le relazioni, quando ci sono, sono complessivamente discrete, meglio quelle di Cremonese e Pecchi. 


Relazione	VOTO	Processo	Prodotto	Relazione+contributo
Cremonese	8,5	8	8,1	8
Dondi		7	6	8,1	7
Pecchi		7,5	6	8,1	7,5
Pietrafitta	7	7	8,1	6
Facchini	6,5	8	8,1	5,5






